﻿var vectorsR3 =
       {
           _x: 1,
           _y: 0,
           _z: 0,
           create: function (x, y, z) {
               var obj = Object.create(this);
               obj.setX(x);
               obj.setY(y);
               obj.setY(z);
               return obj;
           },
           getX: function () {
               return this._x;
           },
           getY: function () {
               return this._y;
           },
           getZ: function () {
               return this._z;
           },
           setX: function (val) {
               this._x = val;
           },
           setY: function (val) {
               this._y = val;
           },
           setZ: function (val) {
               this._z = val;
           },

           setAngle: function (angleX, angleY) {
               var length = this.getLength();
               var length_H = this.getLength() * Math.sin(angleY);

               this._x = length_H * Math.cos(angleX);
               this._y = length * Math.cos(angleY);
               this._z = length_H * Math.sin(angleX);
           },
           setLength: function (length) {
               var angleX = this.getAngleX();
               var angleY = this.getAngleY();
               var length_H = length * Math.sin(angleY);

               this._x = length_H * Math.cos(angleX);
               this._y = length * Math.cos(angleY);
               this._z = length_H * Math.sin(angleX);
           },
           getLength: function () {
               return Math.sqrt(this._x * this._x + this._y * this._y + this._z * this._z);
           },
           getAngleX: function () {
               return Math.acos((this._x / this.getLength()));
           },
           getAngleY: function () {
               return Math.acos((this._y / this.getLength()));
           },
           getAngleZ: function () {
               return Math.acos((this._z / this.getLength()));
           },
           addTo: function (v3) {
               this._x += v3.getX();
               this._y += v3.getY();
               this._z += v3.getZ();
           }




       }