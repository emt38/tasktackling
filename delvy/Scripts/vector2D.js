﻿var vector =
       {
           _x: 1,
           _y: 0,
           create: function (x, y) {
               var obj = Object.create(this);
               obj.setX(x);
               obj.setY(y);
               return obj;
           },
           getX: function () {
               return this._x;
           },
           getY: function () {
               return this._y;
           },
           setX: function (val) {
               this._x = val;
           },
           setY: function (val) {
               this._y = val;
           },

           setAngle: function (angle) {
               var length = this.getLength();
               this._x = length * Math.cos(angle);
               this._y = length * Math.sin(angle);
           },
           setLength: function (length) {
               var angle = this.getAngle();
               this._x = length * Math.cos(angle);
               this._y = length * Math.sin(angle);
           },
           getLength: function () {
               return Math.sqrt(this._x * this._x + this._y * this._y);
           },
           getAngle: function () {
               return Math.atan2(this._y, this._x);
           },
           addTo: function (v2) {
               this._x += v2.getX();
               this._y += v2.getY();
           }

       }