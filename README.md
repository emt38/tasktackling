# README #

This Repository contains the web animation task assigned by Michael Bausano to their DR Applicants.

### How to get around ###

	- Ezequiel's version is stored in the /ezequiel/ directory, and it consists of two different approches to the task (with/without JS)
	- Delvy Ortega's version is stored in the /delvy/ directory, and it's basically the same approaches as above, but with different functionalities.

### How do I get set up? ###

	- Just clone or download the repo and test the .html pages found in the directories

### Ezequiel's project insights /ezequiel/ ###

My project consists of two different approaches: one only using CSS (TaskCSS.html) and one using mostly javascript (TaskJS.html, which is the main one).

I used a library I had made some time ago named Gamelib.js that's mainly composed of a component prototype, which has properties that describe a rectangular component on an absolute plain (x,y,width,height, etc.) and a GameEngine class that uses a canvas rendering context to do functions like rendering
different components (which have to be added to the engine), handling FPS (by using setInterval) and 
calculating DeltaTime for each update execution.

In animation.js (which has the task's specific code) I created two different components, each one representing a rectangle, they both have update function's moving them from one direction to another by incrementing/decrementing their x axis when they hit a boundary.

To create the stripes I use a for loop that's based on the current window size (all stripes are generated over again whenever the window is resized) and the rectangle-to-stripe-width ratio.

Also, I wanted to create two sliders; one handling the FPS (rectangles will move at the same speed, since their traslation is calculated with the DeltaTime) and another handling the rectangle width (all shapes change sized since their based on a proportion of the rectangle's width).



 

### Delvy's project insights /delvy/ ###
My project consists of two different approaches too: one is only using CSS (withCSS.html) and one is using mostly java script (with2DJavaScript.html, which is the main one).

In (withCSS.html) I used a  gradient trick with gradients to simulate the stripe vars and animate 2 div which translate side to side in an infinity loop, the simplest way I found.

In (with2DJavaScript.html) this one I wanted to create a configurable and interactive animation (trying to keep it simple), I used the canvas as main workspace with a 2d context;I started creating an area (with canvas, context and setInterval) in which I could render figures them  I create component that has some basic properties, like (x, y, width, height, etc), after that, I created a start function to initialize all the components.

With the component function I created the stripe vars, they width are based the on 2/3 of the rectangle's width and, they are separated by themselves width between the last one and the current one in a loop. 

The rectangles move by a vector, I decided to use vectors because they allow me to set angles which I can move the rectangles in obtuse angles, applying sin, cos, etc math functions.

Also, I created a function to rotate the canvas from vertical to horizontal and vice versa by switching the canvas and the component properties (e.g., X switch with Y and WIDTH switch with HEIGHT).