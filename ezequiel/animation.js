function updateFPS() {
    setTimeout(() => {
        const fps = Number(document.getElementById('fpsSlider').value);
        document.getElementById('fpsMeter').innerText = fps.toString() + "FPS";
        engine.stop();
        engine.run(fps);
    }, 100);
}

function updateRectangles() {
    document.getElementById('rectangleSizeMeter').innerText = document.getElementById('rectangleWidth').value.toString() + "px";
    init();
}

const canvas = document.getElementById('box');

const StripeColors = {
    ODD: '#FFFFFF',
    EVEN: '#000000'
};

const WidthProportionalLimit = 0.93; // Sometimes the full window size goes over bounds
const HeightProportionalLimit = 0.70; // Sometimes the full window size goes over bounds
const RectangleWidthToHeightRatio = 1/4;
const StripeWidthToRectangleWidthRatio = 1/3;
const RectangleWidthToSpeedRatio = 2/3;

const engine = new GameEngine(canvas);

const background = new Component('bg', 0, 0, 0, 0, 'transparent', 2);
const rectangle1 = new Component('rectangle1', 0, 0, 0, 0, '#030396');
const rectangle2 = new Component('rectangle2', 0, 0, 0, 0, '#FFFF00');

rectangle1.update = () => {
        if((rectangle1.x + rectangle1.width) >= canvas.width) {
            rectangle1.directionFlag = false;
            rectangle1.x = canvas.width - rectangle1.width;
        }
        else if(rectangle1.x <= 0) {
            rectangle1.directionFlag = true;
            rectangle1.x = 0;
        }

        rectangle1.x += Math.ceil((rectangle1.directionFlag ? rectangle1.speed : -rectangle1.speed) * engine.deltaTime); 
};

rectangle2.update = () => {
    if((rectangle2.x + rectangle2.width) >= canvas.width) {
        rectangle2.directionFlag = false;
        rectangle2.x = canvas.width - rectangle1.width;
    }
    else if(rectangle2.x <= 0) {
        rectangle2.directionFlag = true;
        rectangle2.x = 0;
    }

    rectangle2.x += Math.ceil((rectangle2.directionFlag ? rectangle2.speed : -rectangle2.speed) * engine.deltaTime);   
};

engine.add(rectangle1);
engine.add(rectangle2);

const init = () => {
    engine.stop();
    resizeCanvasToWindow();

    const selectedWidth = Number(document.getElementById('rectangleWidth').value);

    const rectangleWidth = Math.floor(selectedWidth);
    const rectangleHeight = Math.floor(selectedWidth * RectangleWidthToHeightRatio);
    const stripeWidth = Math.floor(selectedWidth * StripeWidthToRectangleWidthRatio);

    reloadStripeComponents(stripeWidth);

    rectangle1.directionFlag = true;
    rectangle1.speed = rectangleWidth * RectangleWidthToSpeedRatio;
    rectangle1.width = rectangleWidth;
    rectangle1.height = rectangleHeight;
    rectangle1.x = 0;
    rectangle1.y = Math.floor((canvas.height - rectangle1.height) / 2);
    // Rectangle #1 will be right above the middle of the canvas

    rectangle2.directionFlag = true;
    rectangle2.speed = rectangleWidth * RectangleWidthToSpeedRatio;
    rectangle2.width = rectangleWidth;
    rectangle2.height = rectangleHeight;
    rectangle2.x = 0;
    rectangle2.y = Math.ceil((canvas.height + rectangle2.height) / 2);
    // Rectangle #1 will be right below the middle of the canvas

    engine.run(Math.floor(Number(document.getElementById('fpsSlider').value)))
};

const resizeCanvasToWindow = () => {
    canvas.width = window.outerWidth * WidthProportionalLimit; 
    canvas.height = window.outerHeight * HeightProportionalLimit;
};

const reloadStripeComponents = stripeWidth => {
    engine.applyToTag('stripe', c => {
        engine.remove(c);
    });

    for(i = 0; (i * stripeWidth) < canvas.width; i++) {
        let newStripe = new Component('stripe' + i, i * stripeWidth, 0, stripeWidth, canvas.height, i % 2 == 0 ? StripeColors.EVEN : StripeColors.ODD, 2);
        newStripe.addTag('stripe');
        engine.add(newStripe);
    }
};

window.onresize = () => {
    init();
};

init();