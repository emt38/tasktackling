var DIRECTION = {
	UP: 0,
	DOWN: 1,
	LEFT: 2,
	RIGHT: 3,
	UPLEFT: 5,
	UPRIGHT: 6,
	DOWNLEFT: 7,
	DOWNRIGHT: 8
};

function genRandom(min, max) {
	return Math.floor(Math.random() * (max-min) + min);
}

function BackgroundComponent(component, img="", speedX=-0.1, speedY=0) {
	backgroundSpeed = 0.1;
    component.image = new Image();
    component.image.src = img;
	component.offsetX = 0;
	component.offsetY = 0;
	component.speedX = speedX;
	component.speedY = speedY;
	component.collider = false;
    component.draw = function () {
        this.parentContext.drawImage(this.image, this.x + this.offsetX, this.y + this.offsetY, this.width, this.height);
		var outsideX = this.width - (this.x - this.offsetX);
		if(outsideX > this.width)
			outsideX = outsideX - this.width * 2;

		var outsideY = this.height - (this.y - this.offsetY);
		if(outsideY > this.height)
			outsideY = outsideY - this.height * 2;

		this.parentContext.drawImage(this.image, outsideX % this.width, outsideY % this.height, this.width, this.height);

		this.offsetX += speedX;
		this.offsetY += speedY;

		if(this.x + this.offsetX <= -this.width)
			this.offsetX = this.x;
		else
			if (this.x + this.offsetX >= this.width)
				this.offsetX = this.x;

		if(this.y + this.offsetY <= -this.height)
			this.offsetY = this.y;
		else
			if (this.y + this.offsetY >= this.height)
				this.offsetY = this.y;
    };

    return component;
}

function ImageComponent(component, img="") {

    component.image = new Image();
    component.image.src = img;
    component.draw = function () {
        this.parentContext.drawImage(this.image, this.x, this.y, this.width, this.height);
    };

    return component;
}

function TextComponent(component, t="Message", f="Consolas", fs="16px") {

    component.font = f;
    component.text = t;
    component.fontSize = fs;

    component.draw = function () {
        this.parentContext.font = this.fontSize + " " + this.font;
        this.parentContext.fillStyle = this.color;
        this.parentContext.fillText(this.text, this.x, this.y);
    };

    return component;
}

function Component(id, nx, ny, nb, nh, c, nz=1, col=false, applyGrav=false) {
	this.applyGravity = applyGrav;
	this.gravitySpeed = 0;
	this.tags = [];
	this.collided = [];
	this.name = id;
	this.x = nx;
	this.y = ny;
	this.z = nz;
	this.width = nb;
	this.height = nh;
	this.color = c;
	this.parentCanvas = null;
	this.parentContext = null;
	this.parentEngine = null;
	this.collider = col;
	this.audioListener = new Audio();
	
	this.addTag = function(t) {
		var existing = this.tags.find(function(x) { return x == t; });
		if(!existing && typeof t === "string")
			this.tags.push(t);
	};
	this.hasTag = function(t) {
		if(typeof t !== "string")
			return false;

		res = this.tags.filter(function(a) { return a == t; });
		return (res.length > 0);
	};
	this.removeTag = function(t) {
		for(var i = 0; i < this.tags.length; i++) {
			if(t == this.tags[i]) {
				this.tags.splice(i, 1);
			}
		}
	};
	this.draw = function() {
		this.parentContext.fillStyle = this.color;
		this.parentContext.fillRect(this.x, this.y, this.width, this.height);
	}
	this.keyDown = function() {};
	this.keyPress = function() {};
	this.keyUp = function() {};
	this.click = function() {};
	this.update = function() {};
	this.protoCollision = function(c) {
		if(this.collided.find(function(k) {return k == c.name}))
			this.collision(c);
		else {
			this.collided.push(c.name);
			this.collisionStart(c);
		}
	};
	this.protoCollisionEnd = function(c) {
		for(var i = 0; i < this.collided.length; i++) {
			if(c.name == this.collided[i]) {
				this.collided.splice(i, 1);
				this.collisionEnd(c);
			}
		}
	};
	this.collision = function() {};
	this.collisionStart = function() {};
	this.collisionEnd = function() {};
	this.loadAudio = function(as) {
		this.audioListener.src = as;
		this.audioListener.load();
	};
	this.playAudio = function() {
		this.audioListener.play();
	};
	this.setVolume = function(v) {
		if(v < 0)
			v = 0;
		else if(v > 100)
			v = 100;
		
		this.audioListener.volume = v / 100.0;
	};
}

function keyPressController(c, left=37, up=38, right=39, down=40, speed=1) {
	if(!(c instanceof Component))
		return;
		
	c.keyPress = function() {
		if(typeof(speed) === 'number') {
			if(c.parentEngine.isKeyPressed(left)) {
				c.x -= speed;
				if(c.x < 0)
					c.x = 0;
			}
			if(c.parentEngine.isKeyPressed(right)) {
				c.x += speed;
				if((c.x + c.width) > c.parentCanvas.width)
					c.x = c.parentCanvas.width - c.width;
			}
			if(c.parentEngine.isKeyPressed(up)) {
				c.y -= speed;
				if(c.y < 0)
					c.y = 0;
			}
			if(c.parentEngine.isKeyPressed(down) && (c.y + c.height + speed) < c.parentCanvas.height) {
				c.y += speed;
				if((c.y + c.height) > c.parentCanvas.height)
					c.y = c.parentCanvas.height - c.height;
			}
		} 
		else {
			if(c.parentEngine.isKeyPressed(left)) {
				c.x -= c[speed];
				if(c.x < 0)
					c.x = 0;
			}
			if(c.parentEngine.isKeyPressed(right)) {
				c.x += c[speed];
				if((c.x + c.width) > c.parentCanvas.width)
					c.x = c.parentCanvas.width - c.width;
			}
			if(c.parentEngine.isKeyPressed(up)) {
				c.y -= c[speed];
				if(c.y < 0)
					c.y = 0;
			}
			if(c.parentEngine.isKeyPressed(down)) {
				c.y += c[speed];
				if((c.y + c.height) > c.parentCanvas.height)
					c.y = c.parentCanvas.height - c.height;
			}
		}
			
	};
	
	return;
}

function keyUpController(c, left=37, up=38, right=39, down=40, speed=1) {
	if(!(c instanceof Component))
		return;
	
	
	c.keyUp = function(e) {
		if(typeof(speed) === 'number') {
			if(e.keyCode == left) {
				c.x -= speed;
				if(c.x < 0)
					c.x = 0;
			}
			if(e.keyCode == right) {
				c.x += speed;
				if((c.x + c.width) > c.parentCanvas.width)
					c.x = c.parentCanvas.width - c.width;
			}
			if(e.keyCode == up) {
				c.y -= speed;
				if(c.y < 0)
					c.y = 0;
			}
			if(e.keyCode == down) {
				c.y += speed;
				if((c.y + c.height) > c.parentCanvas.height)
					c.y = c.parentCanvas.height - c.height;
			}
		} 
		else {
			if(e.keyCode == left) {
				c.x -= c[speed];
				if(c.x < 0)
					c.x = 0;
			}
			if(e.keyCode == right) {
				c.x += c[speed];
				if((c.x + c.width) > c.parentCanvas.width)
					c.x = c.parentCanvas.width - c.width;
			}
			if(e.keyCode == up) {
				c.y -= c[speed];
				if(c.y < 0)
					c.y = 0;
			}
			if(e.keyCode == down) {
				c.y += c[speed];
				if((c.y + c.height) > c.parentCanvas.height)
					c.y = c.parentCanvas.height - c.height;
			}
		}
	};
}

function keyDownController(c, left=37, up=38, right=39, down=40, speed=1) {
	if(!(c instanceof Component))
		return;
	
	
	c.keyDown = function(e) {
		if(typeof(speed) === 'number') {
			if(e.keyCode == left) {
				c.x -= speed;
				if(c.x < 0)
					c.x = 0;
			}
			if(e.keyCode == right) {
				c.x += speed;
				if((c.x + c.width) > c.parentCanvas.width)
					c.x = c.parentCanvas.width - c.width;
			}
			if(e.keyCode == up) {
				c.y -= speed;
				if(c.y < 0)
					c.y = 0;
			}
			if(e.keyCode == down) {
				c.y += speed;
				if((c.y + c.height) > c.parentCanvas.height)
					c.y = c.parentCanvas.height - c.height;
			}
		} 
		else {
			if(e.keyCode == left) {
				c.x -= c[speed];
				if(c.x < 0)
					c.x = 0;
			}
			if(e.keyCode == right) {
				c.x += c[speed];
				if((c.x + c.width) > c.parentCanvas.width)
					c.x = c.parentCanvas.width - c.width;
			}
			if(e.keyCode == up) {
				c.y -= c[speed];
				if(c.y < 0)
					c.y = 0;
			}
			if(e.keyCode == down) {
				c.y += c[speed];
				if((c.y + c.height) > c.parentCanvas.height)
					c.y = c.parentCanvas.height - c.height;
			}
		}
	};
}

function GameEngine(canvas) {
	this.gravityVerticalVelocity = 1;
	this.gravityHorizontalVelocity = 1;
	this.gravityDir = DIRECTION.DOWN;
	this.refDate = new Date();
	this.deltaTime = null;
	this.background = "#FFFFFF";
	this.canvas = canvas;
	this.context = canvas.getContext("2d");
	this.components = [];
	this.pressedKeys = [];
	this.canvas.components = this.components;
	this.canvas.engine = this;
	window.game = this;
	this.update = function() {};
	
	this.run = function(f = 60) {
		this.deltaTime = 0;
		this.refDate = new Date();
		this.gameRun = setInterval(function() { this.game.protoUpdate(); }, 1000/f);
	};
	
	this.stop = function() {
	    clearInterval(this.gameRun);

	    setTimeout(function () {
	        this.game.clear();
	        this.game.components.forEach(function(x) {
	            x.draw();
	        });
	    }, 100);
		
	};
	
	this.get = function(name) {
		return this.components.find(function(c) { return c.name == name; });
	};
	this.apply = function(ap) {
		this.components.forEach(function(x) { ap(x);});
	};
	this.applyToName = function(name, ap) {
		var c = this.get(name);
		if(c != null)
			ap(name);
	};
	this.applyToTag = function(tag, ap) {
		var tagged = this.components.filter(function(x) { return x.hasTag(tag);});
		if(tagged.length > 0)
			tagged.forEach(function(c) { ap(c); });
	};
	this.add = function(c) {
		var val = this.components.filter(function(d) { return d.name == c.name });
		if(val.length > 0 || !(c instanceof Component))
			return;
		
		c.parentEngine = this;
		c.parentCanvas = this.canvas;
		c.parentContext = this.context;
		this.components.push(c);
		this.components.sort(function(a, b) { return b.z - a.z; });
	};
	this.remove = function(c) {
		for(i = 0; i < this.components.length; i++) {
			if(this.components[i] == c || this.components[i].name == c.name || this.components[i].name == c)
				this.components.splice(i, 1);
		}
	};
	this.removeKey = function (k) {
		for(i = 0; i < this.pressedKeys.length; i++) {
			if(this.pressedKeys[i] == k) {
				dele = true;
				this.pressedKeys.splice(i, 1);
			   	break;
			}
		}
	};
	this.clear = function() {
		this.context.fillStyle = this.background;
		this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
	};
	this.protoUpdate = function() {
		this.update();
		this.clear();
		var pasada = this.refDate.getTime();
		this.refDate = new Date();
		this.deltaTime = (this.refDate.getTime() - pasada) / 1000;

		this.components.forEach(function(x) {
			if(this.game.pressedKeys.length > 0) {
				x.keyPress();
			}
			x.draw();
			this.game.checkCollisions();
			x.update();
		});
	};
	
	window.onkeydown = function(e) {
		var posIndex = this.game.pressedKeys.findIndex(function(a) { return a == e.keyCode; });
		if(posIndex != -1)
			return;
		this.game.pressedKeys.push(e.keyCode);
		this.game.components.forEach(function(x) {
			x.keyDown(e);
		});
	};
	window.onkeyup = function(e) {
		this.game.removeKey(e.keyCode);
		this.game.components.forEach(function(x) {
			x.keyUp(e);
		});
	};
	this.canvas.onclick = function(e) {
		this.components.forEach(function(x) {
			if(componentClicked(x, e.offsetX, e.offsetY))
				x.click(e);
		});
	};
	this.isKeyPressed = function(k) {
		var res = false;
		this.pressedKeys.forEach(function(c) {
			if(c == k)
				res = true;
		});
		return res;
	};
	this.checkCollisions = function() {
		var cols = this.components.filter(function(e) { return e.collider; });
		cols.forEach(function(c) {
			cols.filter(function(x) { return c.name != x.name; }).forEach(function(x) {
				if(collides(c, x))
					c.protoCollision(x);
				else
					c.protoCollisionEnd(x);
			});
		});
	};
	

	function componentClicked(c, x, y) {
		return ((c.x <= x && x <= (c.x + c.width) ) && (c.y <= y && y <= (c.y + c.height)));
	}

	function collides(c1, c2) {
		if( (((c1.x + c1.width) > c2.x) && (c1.x < (c2.x + c2.width)))
		    && (((c1.y + c1.height) > c2.y) && (c1.y < (c2.y + c2.height)))
		    || (((c2.x + c2.width) > c1.x) && (c2.x < (c1.x + c1.width)))
		    && (((c2.y + c2.height) > c1.y) && (c2.y < (c1.y + c1.height)))
		  ) return true;

		return false;
	};
}